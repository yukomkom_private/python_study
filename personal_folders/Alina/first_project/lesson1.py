# Integer - 0, 22,  12467, -345566556, 2
# Float - 0.2354, 2324344.3, -1.5, 0.0
# String - "Maria", "Alina", "Shco nebyd"

#LESSON-1
f = 0.56 #float
s = "test" #string
t = "aaa"
result=(f+22-56+0)
print(s+" "+t)
print(f)
print("result =", result)
print("some result =",result+result, "lya-lya-lya", sep='')

######HOMEWORK###########################################
#1)
#Declare integer type, floating point type, string type.
variable_integer = 22
variable_flot = 22.999
variable_string = "some text"
#2)
# Subtract 2 or more integer numbers and print result in the consol
first_number_int=50
second_number_int=100
result_int=first_number_int-second_number_int
print("result substract =", result_int)
#3)
# Multiply 2 or more  floating point numbers and print result in the consol
first_number_float=50.55
second_number_float=100.88
result_float=first_number_float*second_number_float
print("result multiply =", result_float)
#4)
# Add 2 or more string and print result in the consol
string_1="hello"
string_2="Yuriy"
print ("result string1 =", string_1+string_2)
print ("result strings =", string_1+" "+string_2)
#5)
# Divide 2 or more integer numbers and print result in the consol
int_1=50
int_2=10
print ("result_devide", int_1/int_2)
#6)
# Divide 25 to 11 then multiply on 4 then add 8 then subtract 13 and multiply to 22
a=((25/11)*4+8-13)*22
print ("Resalt_task 6 = ", a)
#7)
#Divide 49 to 7 and add 26
b=(49/7)+26
print ("Resalt_task 7 = ", b)
#8)
#Module from 2 numbers that result equals 7 and get module from another 2 number that result equals 1
val_1=25
val_2=9
result_task_8_part1=val_1%val_2
print ("Result-1_task 8 = ", result_task_8_part1)
val_3=5
val_4=4
result_task_8_part2=val_3%val_4
print ("Result-2_task 8 = ", result_task_8_part2)
#9)
# Raise 3 to the power 3 and raise 12 to the power 4
val_5=3
val_6=3
result_task_9_patrt1=val_5**val_6
print ("Result-1_task 9 = ", result_task_9_patrt1)
val_7=12
val_8=4
result_task_9_patrt2=val_7**val_8
print ("Result-2_task 9 = ", result_task_9_patrt2)
#10)
## Try to understand last operator Floor Division //, how it works and write some sample code
val_1=23
val_2=5
val_3=-5
# we need to receive result of division where the digits after the decimal point will be removed
result_task_10_patrt1=val_1//val_2
print ("Result-1_task 10 = ", result_task_10_patrt1)
# we need to receive result of division where the result will be floored
result_task_10_patrt2=val_1//val_3
print ("Result-1_task 10 = ", result_task_10_patrt2)



