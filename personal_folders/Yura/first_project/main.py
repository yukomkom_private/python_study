print("#==================================================TASK 1=====================================================#")

# 1. Declares int, floating point, string types
integer_type = 25
float_type = 12.4
string_type = "Some dummy string for example"

# 2. Subtracts and prints ints result
some_int_var1_subtract_var2_result = 1001 - 232
print(some_int_var1_subtract_var2_result)

# 3. Multiplies and prints floats result
some_float_var1 = 0.04
some_float_var2 = 1.21
print(some_float_var1 * some_float_var2)

# 4. Adds and prints strings result
some_string_var1 = "Hello "
some_string_var2 = "that"
some_string_var3 = " mysterious "
some_string_var4 = "Python World!!!"
some_strings_result = some_string_var1 + some_string_var2 + some_string_var3 + some_string_var4
print(some_strings_result)

# 5. Divides and prints result
print(1001 / 232)

# 6*. Some mathematical operations
some_mathematical_operations_result = (25 / 11 * 4 + 8 - 13) * 22
print(some_mathematical_operations_result)

# 7. Some mathematical operations
some_mathematical_operations_result = 49 / 7 + 26
print(some_mathematical_operations_result)

# 8*. Module example
module_equal_seven = 31 % 12
print(module_equal_seven)
module_equal_one = 23 % 2
print(module_equal_one)

# 9*. Raise to Power
three_raise_to_power_trhee = 3 ** 3
print(three_raise_to_power_trhee)
twelve_raise_to_power_four = 12 ** 4
print(twelve_raise_to_power_four)

# 10**. Floor Division operator //
print(12 // 5)
print(22 // -10)
print(-22 // -10)

print("#==================================================TASK 2=====================================================#")

#0. Convert int into string, int into float and float into string, please use different values for each convertation
int_var_1 = 25
converted_int_to_string = str(int_var_1)
int_var_2 = 188
converted_int_to_float = float(int_var_2)
float_val_1 = 3454.545
converted_float_to_string = str(float_val_1)
print("Result: " + converted_int_to_string + " " + str(converted_int_to_float) + " " + converted_float_to_string)


#1. Get number from console input and print it.
input_var = int(input("Please, input number: "))
print(input_var)

#2. Declare boolen values and print them.
boolen_true = True
boolen_false = False
print("There are only 2 boolen values:", boolen_true, boolen_false)

#3. Get number from console input and check if it greater than 100.
if input_var > 100:
    print("Input is greater than 100")

#4. Get number from console input and check if it divided by 2 is greater than 4. Print result.
if (input_var / 2) > 4:
    print(input_var, "divided by 2 is greater than 4")

#5*. You have numbers 10, 24, 43, 2. Print least, greatest and addition result of 2 middle numbers into console.
var10 = 10
var24 = 24
var43 = 43
var2 = 2

# Lhe least one
least = var10
if least > var24:
    least = var24
if least > var43:
    least = var43
if least > var2:
    least = var2
print("The least one is:", least)

# The greatest one
greatest = var10
if greatest < var24:
    greatest = var24
if greatest < var43:
    greatest = var43
if greatest < var2:
    greatest = var2
print("The greatest one is:", greatest)

# Addition
result = var10 + var24 + var43 + var2 - least - greatest
print("Addition result:", result)

#6. Get number from console input then check step by step if its greater than 10, less than -25 and it's
#   module(% opretator) from 8 is greater equal 4, on each successfull copmarison, print result into console.
if input_var >= 10:
    print("Input is greater than 10")
if input_var < -25:
    print("Input is less than -25")
if (input_var > 8) and (input_var % 8) >= 4:
    print("Input after module 8 is greater than 4")

#7*. Use short form of compare operator if-else and declare integer variable 'var' depending on integer value inputed
#    from consol. If value less equal 10 'var' must become equal 5, if not than 25. In the end print result.
var = 5 if input_var else 25
print("Short form is-else operator is:", var)

#8**. Get number from console input than check in one 'if' statement if it is greater than 10, less than 100 and
#    it's module(% operator) from 7 was less 3. Print result.
if (input_var > 10) and (input_var < 100) and ((input_var > 7) and ((input_var % 7) < 3)):
    print("Condition was passed with value:", input_var)

#9. Compare couple of times integer values using '==', '!=', 'is', 'is not' operators and print results
print("14 == 25, result:", 14 == 25)
print("26 != 25, result:", 26 == 25)
print("145 is not 2, result:", 145 is not 2)
print("145 is 200, result:", 145 is 200)

