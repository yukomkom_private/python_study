petro = 1.9
ivan = "Petro"

big_variable = 222222
zero = 0

result = big_variable / (101 - 10220)
zero = result / 222 * 30
print(zero)

#print("ivan = ", zero, "; " ,"petro = ", result, "; ", "halyna = ", ivan, "; ", sep='')
#1
red = 4 #integer
pink = 3.5 #floating
green = "grey" #string

#2
blue = 25
ivory = 11
result = ivory - blue - red
print("violet = ",result)

#3
yellow = 1.7856936547
black = 123456.9
multiply1 = pink * yellow
multiply2 = multiply1 * black
print("orange = ", multiply1,";")
print("brown = ", multiply2,".")

#4
string1 = "white"
string2 = "nude"
print("The dress is", green,"," , "the stocks are", string1,"," , "and the lipstick is", string2,".")

#5
gold = red / blue / ivory
print("silver =",gold,";" )

#6
beige = 8
turquoise = 13
vinous = 22
result1 = (((((blue / ivory) * red)) + beige) - turquoise) * vinous
print("bronze =",result1)
result2 = (blue / ivory * red + beige - turquoise) * vinous
print("coral =",result2)

#7
crimson = 49
emerald = 7
fuchsia = 26
result3 = crimson / emerald + fuchsia
print("indigo =",result3)

#8
jade = 15
lavender = 8
lemon = 81
lilac = 2
result4 = jade%lavender
result5 = lemon%lilac
print("khaki =",result4,";", result5,".")

#9
lime = 3
linen = 12
result6 = lime ** lime
result7 = linen ** red
print("maroon =",result6,";",result7)

#10
result8 = linen // lemon
print("ochre =",result8)
